from src import utils

def test_is_prime() :
    assert utils.is_prime(4) == False
    assert utils.is_prime(2) == True
    assert utils.is_prime(3) == True
    assert utils.is_prime(8) == False
    assert utils.is_prime(10) == False
    assert utils.is_prime(7) == True
    assert utils.is_prime(1) == False
    assert utils.is_prime(-3) == "Negative numbers are not allowed"

def test_cubic():
    assert utils.cubic(2) == 8
    assert utils.cubic(-2) == -8
    assert utils.cubic(2) != 4
    assert utils.cubic(-3) != 27

def test_say_name():
    assert utils.say_hello("Alexandre") == "Hello, Alexandre"
    assert utils.say_hello("Christopher") == "Hello, Christopher"
    assert utils.say_hello("Romain") != "Hi, Alexandre"
    assert utils.say_hello("Alexandre") != "Hi , prout"